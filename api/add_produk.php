<?php 

    require "config/connect.php";

    $data = json_decode(file_get_contents("php://input"), true);

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        $response       = array();
        $sku            = $_POST['sku'];
        $nama_produk    = $_POST['nama_produk'];
        $stock          = $_POST['stock'];
        $harga          = $_POST['harga'];

        $fileName       =  $_FILES['gambar']['name'];
        $tempPath       =  $_FILES['gambar']['tmp_name'];
        $fileSize       =  $_FILES['gambar']['size'];

        $noSpaceFileName   = str_replace(" ","",$fileName);

        if (empty($fileName)) {
            $response['status'] = "gagal";
            $response['pesan']  = "Gambar produk harus diisi.";
        }else{
            $upload_path = '../uploads/';
            $fileExt = strtolower(pathinfo($fileName,PATHINFO_EXTENSION));
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); 

            $newFileName = $sku."-".$noSpaceFileName;

            if(in_array($fileExt, $valid_extensions)){

                if($fileSize < 5000000){
                    move_uploaded_file($tempPath, $upload_path . $newFileName);

                    $sql    = "INSERT INTO produk VALUE(NULL, '$sku', '$nama_produk', '$stock', '$harga', '$newFileName')";
                    $insert = mysqli_query($connect, $sql);

                    if ($insert) {
                        $response['status'] = "sukses";
                        $response['pesan']  = "Data berhasil disimpan.";
                    }else{
                        $response['status'] = "gagal";
                        $response['pesan']  = "Data gagal disimpan.";
                    }

                }
                else{	
                    $response['status'] = "gagal";
                    $response['pesan']  = "Ukuran gambar terlalu besar.";	
                }
                
            }else{				
                $response['status'] = "gagal";
                $response['pesan']  = "Format gambar yang tersedia JPG, JPEG, PNG dan GIF.";
            }

        }

        echo json_encode($response);
    }

?>
