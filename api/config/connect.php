<?php 

    define('HOST', 'localhost');
    define('USER', 'root');
    define('PASSWORD', '');
    define('DB', 'unhi_mp_db');

    $connect = mysqli_connect(HOST, USER, PASSWORD, DB) or die ('Tidak bisa terhubung dengan database.');

    header("Content-Type: application/json");
    header("Acess-Control-Allow-Origin: *");
    header("Acess-Control-Allow-Methods: POST");
    header("Acess-Control-Allow-Headers: Acess-Control-Allow-Headers,Content-Type,Acess-Control-Allow-Methods, Authorization");

?>