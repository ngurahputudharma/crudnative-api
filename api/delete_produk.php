<?php 

    require "config/connect.php";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        $response       = array();

        $produk_id      = $_POST['produk_id'];

        $sql_cek    = "SELECT * FROM produk WHERE id = '$produk_id'";
        $cek_data   = mysqli_fetch_array(mysqli_query($connect, $sql_cek));
        $old_gambar = $cek_data['gambar'];

        $upload_path = '../uploads/';
        unlink($upload_path.$old_gambar);

        $sql    = "DELETE FROM produk WHERE id = '$produk_id'";
        $delete = mysqli_query($connect, $sql);

        if ($delete) {
            $response['status'] = "sukses";
            $response['pesan']  = "Data berhasil dihapus.";
        }else{
            $response['status'] = "gagal";
            $response['pesan']  = "Data gagal dihapus.";
        }

        echo json_encode($response);
    }

?>