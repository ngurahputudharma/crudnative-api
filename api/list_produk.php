<?php 

    require "config/connect.php";

        
    $response       = array();

    $sql    = "SELECT * FROM produk";
    $list   = mysqli_query($connect, $sql);

    while ($data = mysqli_fetch_array($list)) {
        $produk['id']           = $data['id'];
        $produk['sku']          = $data['sku'];
        $produk['nama_produk']  = $data['nama_produk'];
        $produk['stock']        = $data['stock'];
        $produk['harga']        = $data['harga'];
        $produk['gambar']       = $data['gambar'];

        array_push($response, $produk);
    }

    echo json_encode($response);
    
?>