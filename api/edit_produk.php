<?php 

    require "config/connect.php";

    $data = json_decode(file_get_contents("php://input"), true);

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        $response       = array();
        $sku            = $_POST['sku'];
        $nama_produk    = $_POST['nama_produk'];
        $stock          = $_POST['stock'];
        $harga          = $_POST['harga'];
        $produk_id      = $_POST['produk_id'];

        $fileName       =  $_FILES['gambar']['name'];
        $tempPath       =  $_FILES['gambar']['tmp_name'];
        $fileSize       =  $_FILES['gambar']['size'];

        $sql_cek    = "SELECT * FROM produk WHERE id = '$produk_id'";
        $cek_data   = mysqli_fetch_array(mysqli_query($connect, $sql_cek));
        $old_gambar = $cek_data['gambar'];

        $noSpaceFileName   = str_replace(" ","",$fileName);

        if (empty($fileName)) {
            // $response['status'] = "gagal";
            // $response['pesan']  = "Gambar produk harus diisi.";
            $newFileName = $old_gambar;
        }else{

            $upload_path = '../uploads/';

            unlink($upload_path.$old_gambar);

            $fileExt = strtolower(pathinfo($fileName,PATHINFO_EXTENSION));
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); 

            $newFileName = $sku."-".$noSpaceFileName;

            if(in_array($fileExt, $valid_extensions)){

                if($fileSize < 5000000){
                    move_uploaded_file($tempPath, $upload_path . $newFileName);
                }
                else{	
                    $response['status'] = "gagal";
                    $response['pesan']  = "Ukuran gambar terlalu besar.";	
                }
                
            }else{				
                $response['status'] = "gagal";
                $response['pesan']  = "Format gambar yang tersedia JPG, JPEG, PNG dan GIF.";
            }

        }

        $sql    = "UPDATE produk SET sku = '$sku', nama_produk = '$nama_produk', stock = '$stock', harga = '$harga', gambar = '$newFileName' WHERE id = '$produk_id'";
        $update = mysqli_query($connect, $sql);

        if ($update) {
            $response['status'] = "sukses";
            $response['pesan']  = "Data berhasil disimpan.";
        }else{
            $response['status'] = "gagal";
            $response['pesan']  = "Data gagal disimpan.";
        }

        echo json_encode($response);
    }

?>